#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    move_teei_data.sh \

PRODUCT_PACKAGES += \
    fstab.mt6763 \
    init.project.rc \
    init.modem.rc \
    factory_init.project.rc \
    init.stnfc.rc \
    init.connectivity.rc \
    init.ago.rc \
    meta_init.modem.rc \
    init.sensor_1_0.rc \
    meta_init.connectivity.rc \
    init.mt6763.rc \
    factory_init.connectivity.rc \
    init.aee.rc \
    factory_init.rc \
    meta_init.project.rc \
    multi_init.rc \
    init.mt6763.usb.rc \
    meta_init.rc \
    ueventd.rc \
    init.recovery.mt6763.rc \
    init.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 28

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/gigaset/GX290/GX290-vendor.mk)
