#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_GX290.mk

COMMON_LUNCH_CHOICES := \
    lineage_GX290-user \
    lineage_GX290-userdebug \
    lineage_GX290-eng
