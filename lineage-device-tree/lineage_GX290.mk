#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from GX290 device
$(call inherit-product, device/gigaset/GX290/device.mk)

PRODUCT_DEVICE := GX290
PRODUCT_NAME := lineage_GX290
PRODUCT_BRAND := Gigaset
PRODUCT_MODEL := GX290
PRODUCT_MANUFACTURER := gigaset

PRODUCT_GMS_CLIENTID_BASE := android-gigaset

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_k63v2_64_bsp-user 10 QP1A.190711.020 1597289677 release-keys"

BUILD_FINGERPRINT := Gigaset/GX290_EEA/GX290:10/QP1A.190711.020/1597289677:user/release-keys
