#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_GX290.mk

COMMON_LUNCH_CHOICES := \
    omni_GX290-user \
    omni_GX290-userdebug \
    omni_GX290-eng
