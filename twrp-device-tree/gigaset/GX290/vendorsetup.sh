#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_GX290-user
add_lunch_combo omni_GX290-userdebug
add_lunch_combo omni_GX290-eng
